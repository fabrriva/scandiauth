package net.scandicraft.scandiauth.http;

public class HttpStatus {
    public static int HTTP_OK = 200;
    public static int HTTP_UNAUTHORIZED = 403;
}
